import 'package:flutter_bloc/flutter_bloc.dart';

class CounterBloc extends Bloc<CounterEvent, CounterState> {
  int counter = 0;
  CounterBloc() : super(CounterInitial()) {
    on<CounterEvent>((event, emit) {
      if (event is IncreamentEvent) {
        counter = counter + 2;
        emit(ChangedState(counter: counter));
      } else if (event is DecreamentEvent) {
        counter = counter - 1;
        if (counter <= 0) {
          counter = 0;
        }
        emit(ChangedState(counter: counter));
      }
    });
  }
}

abstract class CounterState {}

class CounterInitial extends CounterState {}

class ChangedState extends CounterState {
  final int counter;
  ChangedState({
    required this.counter,
  });
}

abstract class CounterEvent {}

class IncreamentEvent extends CounterEvent {}

class DecreamentEvent extends CounterEvent {}
