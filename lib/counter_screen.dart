import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter/material.dart';

import 'bloc/bloc_counter.dart';
import 'button_widget.dart';

class CounterScreen extends StatelessWidget {
  const CounterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade800,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'Счетчик:',
              style: TextStyle(fontSize: 20, color: Colors.white),
            ),
            const SizedBox(
              height: 10,
            ),
            const ButtonsWidget(),
            BlocBuilder<CounterBloc, CounterState>(
              builder: (context, state) {
                if (state is CounterInitial) {
                  return const Text(
                    "0",
                    style: TextStyle(color: Colors.white, fontSize: 25),
                  );
                } else if (state is ChangedState) {
                  return Text(
                    state.counter.toString(),
                    style: const TextStyle(color: Colors.white, fontSize: 25),
                  );
                } else {
                  return const SizedBox();
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
